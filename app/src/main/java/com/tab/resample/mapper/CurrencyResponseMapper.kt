package com.tab.resample.mapper

import com.tab.resample.mapper.CurrencyResponseMapper.Companion.DEFAULT_VALUE
import com.tab.resample.repository.CurrencyResponse
import javax.inject.Inject

class CurrencyResponseMapper @Inject constructor() {

    companion object {
        private const val DEFAULT_RATE = 1.0
        const val DEFAULT_VALUE = "-"
    }

    fun mapToMap(currencyResponse: CurrencyResponse): Map<String, Double> {
        val map = linkedMapOf<String, Double>()
        map[currencyResponse.base.orDefault()] = DEFAULT_RATE

        return map.plus(currencyResponse.rates)
    }
}

private fun String.orDefault(): String {
    return if (isEmpty()) DEFAULT_VALUE else this
}