package com.tab.resample.mapper

import com.tab.resample.view.model.CurrencyUiModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.math.BigDecimal
import javax.inject.Inject

class AmountUiMapper @Inject constructor(private val moneyFormatter: MoneyFormatter) {

    suspend fun map(
        amount: BigDecimal,
        currency: String,
        currencyUiList: List<CurrencyUiModel>,
        newCurrencyMap: Map<String, Double>
    ): List<CurrencyUiModel> = withContext(Dispatchers.Default) {
        if (newCurrencyMap.isEmpty()) return@withContext currencyUiList

        return@withContext if (currencyUiList.isNullOrEmpty()) {
            createNewCurrencyList(amount, currency, newCurrencyMap)
        } else {
            updateCurrencyList(amount, currency, currencyUiList, newCurrencyMap)
        }
    }

    private fun createNewCurrencyList(
        amount: BigDecimal,
        currency: String,
        newCurrencyMap: Map<String, Double>
    ): List<CurrencyUiModel> {

        return newCurrencyMap.map {
            val itemCurrency = it.key
            val currencyFullName = moneyFormatter.getFullCurrencyName(itemCurrency)
            val decimalPlacesCount = moneyFormatter.getDecimalPlaces(itemCurrency)

            if (itemCurrency == currency) {
                CurrencyUiModel(
                    currency = itemCurrency,
                    currencyFullName = currencyFullName,
                    calculatedAmount = amount,
                    decimalPlacesCount = decimalPlacesCount,
                    isFirstItem = true
                )
            } else {
                val rate = it.value
                CurrencyUiModel(
                    currency = itemCurrency,
                    currencyFullName = currencyFullName,
                    calculatedAmount = calculateAmount(amount, rate),
                    decimalPlacesCount = decimalPlacesCount
                )
            }
        }
    }

    private fun updateCurrencyList(
        amount: BigDecimal,
        currency: String,
        currencyUiList: List<CurrencyUiModel>,
        newCurrencyMap: Map<String, Double>
    ): List<CurrencyUiModel> {

        return currencyUiList
            .map {
                val itemCurrency = it.currency
                val currencyFullName = moneyFormatter.getFullCurrencyName(itemCurrency)
                val decimalPlacesCount = moneyFormatter.getDecimalPlaces(itemCurrency)

                if (itemCurrency == currency) {
                    CurrencyUiModel(
                        currency = itemCurrency,
                        currencyFullName = currencyFullName,
                        calculatedAmount = amount,
                        decimalPlacesCount = decimalPlacesCount,
                        isFirstItem = true
                    )
                } else {
                    val rate = newCurrencyMap[itemCurrency] ?: 0.0

                    CurrencyUiModel(
                        currency = itemCurrency,
                        currencyFullName = currencyFullName,
                        calculatedAmount = calculateAmount(amount, rate),
                        decimalPlacesCount = decimalPlacesCount
                    )
                }
            }
    }

    private fun calculateAmount(amount: BigDecimal, rate: Double): BigDecimal {
        return if (amount == BigDecimal.ZERO) {
            BigDecimal.ZERO
        } else {
            amount.multiply(BigDecimal(rate))
        }
    }

}
