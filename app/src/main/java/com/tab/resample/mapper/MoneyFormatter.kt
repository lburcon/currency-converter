package com.tab.resample.mapper

import com.tab.resample.view.model.CurrencyUiModel
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*
import javax.inject.Inject

class MoneyFormatter @Inject constructor() {

    companion object {
        private const val DEFAULT_AMOUNT = "0"
        private const val SEPARATOR = "."
        private const val EXCESSIVE_DECIMAL_SIGN = -1
        private const val NO_EXCESSIVE_DECIMALS = 0
    }

    fun format(amount: String, maxDecimalPlaces: Int): String {
        if (amount.isEmpty()) return DEFAULT_AMOUNT

        val excessiveDecimalCount = validateAmount(amount, maxDecimalPlaces)
        val formattedAmount: String

        formattedAmount = when {
            excessiveDecimalCount > 0 -> amount.dropLast(excessiveDecimalCount)
            excessiveDecimalCount == EXCESSIVE_DECIMAL_SIGN -> amount.replace(SEPARATOR, "")
            else -> amount
        }

        return formattedAmount
    }

    private fun validateAmount(amount: String, maxDecimalCount: Int): Int {
        if (amount.contains(SEPARATOR)) {
            if (maxDecimalCount == 0) {
                return EXCESSIVE_DECIMAL_SIGN
            }
            val decimalPart = amount.substringAfter(SEPARATOR)
            if (decimalPart.length > maxDecimalCount) {
                return decimalPart.length - maxDecimalCount
            }
        }
        return NO_EXCESSIVE_DECIMALS
    }

    fun getAmount(item: CurrencyUiModel): String {
        return if (item.calculatedAmount == BigDecimal.ZERO) {
            ""
        } else {
            item.calculatedAmount.setScale(item.decimalPlacesCount, RoundingMode.HALF_UP).toString()
        }
    }

    fun getFullCurrencyName(currency: String): String =
        Currency.getInstance(currency).getDisplayName(Locale.getDefault())

    fun getDecimalPlaces(currency: String) =
        Currency.getInstance(currency).defaultFractionDigits
}