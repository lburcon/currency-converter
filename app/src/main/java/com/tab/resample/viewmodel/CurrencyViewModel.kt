package com.tab.resample.viewmodel

import androidx.lifecycle.MutableLiveData
import com.tab.resample.domain.GetCurrenciesUseCase
import com.tab.resample.mapper.AmountUiMapper
import com.tab.resample.utils.uiJob
import com.tab.resample.view.ViewContract
import com.tab.resample.view.model.CurrencyUiModel
import kotlinx.coroutines.delay
import java.math.BigDecimal
import javax.inject.Inject

class CurrencyViewModel @Inject constructor(
    private val view: ViewContract,
    private val getCurrenciesUseCase: GetCurrenciesUseCase,
    private val amountUiMapper: AmountUiMapper
) : BaseViewModel() {

    companion object {
        private const val DEFAULT_CURRENCY = "EUR"
        private val DEFAULT_AMOUNT = BigDecimal.ZERO
    }

    private var amount: BigDecimal = DEFAULT_AMOUNT
    private var currency: String = DEFAULT_CURRENCY
    private var currencyMap = mapOf<String, Double>()

    private val currencyLiveData: MutableLiveData<List<CurrencyUiModel>> by lazy {
        MutableLiveData<List<CurrencyUiModel>>()
    }

    fun getLiveData() = currencyLiveData

    /**
     * Method invoked on resume of Activity or change of currency. Runs every 1 second
     */
    fun getCurrencyList() {
        currencyMap = mapOf()

        uiJob {
            while (true) {
                val result = getCurrenciesUseCase.execute(currency)
                processResult(result)
                delay(1000)
            }
        }
    }

    /**
     * Method invoked from EditText's listener
     */
    fun amountUpdated(newAmount: BigDecimal) {
        amount = newAmount
        uiJob {
            updateList()
        }
    }

    /**
     * Method invoked when new currency goes to top
     */
    fun currencyChanged(position: Int, currency: String, amount: BigDecimal) {
        this.currency = currency
        this.amount = amount

        if (currencyMap.isNotEmpty()) {
            updateLiveDataPositions(position)
        }

        cancelCoroutines() //stop every 1s downloading and updating OLD currency data
        getCurrencyList()
    }

    fun onStopViewModel() {
        cancelCoroutines()
    }

    private suspend fun updateList() {
        val currentList = currencyLiveData.value ?: emptyList()
        val items = amountUiMapper.map(amount, currency, currentList, currencyMap)
        if (items.isNotEmpty()) {
            currencyLiveData.value = items
        }
    }

    private suspend fun processResult(result: GetCurrenciesUseCase.UseCaseResult) {
        when (result) {
            is GetCurrenciesUseCase.UseCaseResult.Success -> {
                val newMap = mapOf<String, Double>().plus(result.data)
                currencyMap = newMap
                updateList()
            }
            is GetCurrenciesUseCase.UseCaseResult.NetworkError ->
                view.onNetworkError()
            is GetCurrenciesUseCase.UseCaseResult.UnknownError -> {
                /* no-op */
            }

        }
    }

    private fun updateLiveDataPositions(position: Int) {
        val currentList = arrayListOf<CurrencyUiModel>()
        currentList.addAll(currencyLiveData.value ?: emptyList())

        val currencyUiModel = currentList[position]
        currentList.removeAt(position)
        currentList.add(0, currencyUiModel)
        currencyLiveData.value = currentList
    }

}