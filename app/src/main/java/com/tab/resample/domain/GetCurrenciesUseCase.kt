package com.tab.resample.domain

import com.tab.resample.mapper.CurrencyResponseMapper
import com.tab.resample.repository.CurrencyRepository
import com.tab.resample.repository.isSuccess
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.inject.Inject

class GetCurrenciesUseCase @Inject constructor(
    private val currencyRepository: CurrencyRepository,
    private val currencyResponseMapper: CurrencyResponseMapper
) {

    suspend fun execute(currency: String): UseCaseResult = withContext(Dispatchers.Default) {
        try {
            val response = currencyRepository.getCurrencyList(currency)

            if (response.isSuccess()) {
                val data = currencyResponseMapper.mapToMap(response)
                return@withContext UseCaseResult.Success(data)
            }
            UseCaseResult.UnknownError
        } catch (exception: Exception) {
            if (exception is SocketTimeoutException || exception is UnknownHostException) {
                UseCaseResult.NetworkError
            } else {
                UseCaseResult.UnknownError
            }
        }
    }

    sealed class UseCaseResult {
        data class Success(val data: Map<String, Double>) : UseCaseResult()
        object NetworkError : UseCaseResult()
        object UnknownError : UseCaseResult()
    }
}