package com.tab.resample.view.recycler.viewholder

import android.annotation.SuppressLint
import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.tab.resample.mapper.MoneyFormatter
import com.tab.resample.view.model.CurrencyUiModel
import java.math.BigDecimal


abstract class BaseViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

    companion object {
        private const val TYPE_DRAWABLE = "drawable"
        private const val FLAG_ENDING = "_flag"
    }

    abstract fun bind(
        item: CurrencyUiModel,
        amountUpdatedListener: ((amount: BigDecimal) -> Unit)?,
        positionSwapListener: ((position: Int, currency: String, amount: BigDecimal) -> Unit)?,
        position: Int,
        imageAlreadySet: Boolean,
        moneyFormatter: MoneyFormatter,
        glide: RequestManager
    )

    @SuppressLint("DefaultLocale")
    fun setCountryFlag(imageView: ImageView, glide: RequestManager, currency: String) {
        val flagId = getResourceId(currency.toLowerCase() + FLAG_ENDING)
        if (flagId > 0) {
            val flagDrawable = view.context.getDrawable(flagId)

            glide.load(flagDrawable)
                .apply(RequestOptions.circleCropTransform())
                .into(imageView)
        }
    }

    private fun getResourceId(pVariableName: String): Int {
        return try {
            view.resources.getIdentifier(
                pVariableName,
                TYPE_DRAWABLE, view.context.packageName
            )
        } catch (e: Exception) {
            e.printStackTrace()
            -1
        }
    }
}