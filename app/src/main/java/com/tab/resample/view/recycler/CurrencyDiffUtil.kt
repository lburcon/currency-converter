package com.tab.resample.view.recycler

import android.os.Bundle
import androidx.recyclerview.widget.DiffUtil
import com.tab.resample.view.model.CurrencyUiModel
import javax.inject.Inject

class CurrencyDiffUtil @Inject constructor() : DiffUtil.ItemCallback<CurrencyUiModel>() {

    companion object {
        const val IMAGE_ALREADY_SET = "image_downloaded"
    }

    override fun areItemsTheSame(oldItem: CurrencyUiModel, newItem: CurrencyUiModel) =
        oldItem.currency == newItem.currency

    override fun areContentsTheSame(
        oldItem: CurrencyUiModel,
        newItem: CurrencyUiModel
    ): Boolean = oldItem == newItem || newItem.isFirstItem

    override fun getChangePayload(oldItem: CurrencyUiModel, newItem: CurrencyUiModel): Any? {
        val diff = Bundle()
        if (oldItem.currency == newItem.currency) {
            diff.putBoolean(IMAGE_ALREADY_SET, true)
        }
        return diff
    }
}