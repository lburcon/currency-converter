package com.tab.resample.view

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.tab.resample.R
import com.tab.resample.view.model.CurrencyUiModel
import com.tab.resample.view.recycler.CurrencyAdapter
import com.tab.resample.viewmodel.CurrencyViewModel
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject


class MainActivity : DaggerAppCompatActivity(), ViewContract {

    companion object {
        private const val RECYCLER_VIEW_SCROLL_DELAY = 200L
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var adapter: CurrencyAdapter

    private var snackbar: Snackbar? = null

    private val container by lazy {
        findViewById<ViewGroup>(R.id.container)
    }

    private val loadingState by lazy {
        findViewById<ViewGroup>(R.id.loading_status)
    }

    private val recyclerView by lazy {
        findViewById<RecyclerView>(R.id.currency_list)
    }
    private val headerText by lazy {
        findViewById<TextView>(R.id.header_text)
    }

    private val viewModel: CurrencyViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(CurrencyViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel.getLiveData().observe(this@MainActivity, Observer {
            dataReceived(it)
        })

        initialiseRecyclerView()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getCurrencyList()
    }

    override fun onStop() {
        super.onStop()
        viewModel.onStopViewModel()
    }

    override fun onNetworkError() {
        if (snackbar == null) {
            snackbar = Snackbar.make(
                container,
                resources.getString(R.string.error_no_connection),
                Snackbar.LENGTH_INDEFINITE
            ).apply { show() }
        }
    }

    private fun initialiseRecyclerView() {
        adapter.apply {
            setAmountUpdatedListener { newAmount ->
                viewModel.amountUpdated(newAmount)
            }
            setPositionSwapListener { position, currency, amount ->
                viewModel.currencyChanged(position, currency, amount)
                Handler().postDelayed({
                    recyclerView.smoothScrollToPosition(0)
                }, RECYCLER_VIEW_SCROLL_DELAY)
            }
        }

        recyclerView.adapter = adapter
    }

    private fun dataReceived(it: List<CurrencyUiModel>?) {
        snackbar?.dismiss()
        snackbar = null

        adapter.submitList(it)

        loadingState.visibility = View.GONE
        headerText.visibility = View.VISIBLE
    }
}
