package com.tab.resample.view.recycler.viewholder

import android.annotation.SuppressLint
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.RequestManager
import com.tab.resample.R
import com.tab.resample.mapper.MoneyFormatter
import com.tab.resample.view.model.CurrencyUiModel
import java.math.BigDecimal


class CurrencyNotEditableViewHolder(private val view: View) : BaseViewHolder(view) {

    private val currencyText: TextView by lazy {
        view.findViewById<TextView>(R.id.currency_text)
    }
    private val currencyFullText: TextView by lazy {
        view.findViewById<TextView>(R.id.currency_full_text)
    }
    private val amount: EditText by lazy {
        view.findViewById<EditText>(R.id.amount)
    }
    private val countryFlag: ImageView by lazy {
        view.findViewById<ImageView>(R.id.country_flag)
    }

    @SuppressLint("SetTextI18n")
    override fun bind(
        item: CurrencyUiModel,
        amountUpdatedListener: ((amount: BigDecimal) -> Unit)?,
        positionSwapListener: ((position: Int, currency: String, amount: BigDecimal) -> Unit)?,
        position: Int,
        imageAlreadySet: Boolean,
        moneyFormatter: MoneyFormatter,
        glide: RequestManager
    ) {
        currencyText.text = item.currency
        currencyFullText.text = item.currencyFullName

        amount.apply {
            setText(moneyFormatter.getAmount(item))
            isFocusable = false
        }

        if (!imageAlreadySet) {
            setCountryFlag(countryFlag, glide, item.currency)
        }

        view.setOnClickListener {
            positionSwapListener?.invoke(position, item.currency, BigDecimal(convertAmount(amount)))
        }
    }

    private fun convertAmount(amount: EditText): String =
        if (amount.text.isEmpty()) {
            0.toString()
        } else {
            amount.text.toString()
        }
}

