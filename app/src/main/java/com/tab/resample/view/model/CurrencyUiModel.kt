package com.tab.resample.view.model

import java.math.BigDecimal

data class CurrencyUiModel(
    val currency: String,
    val currencyFullName: String,
    val calculatedAmount: BigDecimal,
    val decimalPlacesCount: Int,
    val isFirstItem: Boolean = false
)