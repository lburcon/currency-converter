package com.tab.resample.view.recycler.viewholder

import android.annotation.SuppressLint
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.RequestManager
import com.tab.resample.R
import com.tab.resample.mapper.MoneyFormatter
import com.tab.resample.view.model.CurrencyUiModel
import java.math.BigDecimal


class CurrencyEditableViewHolder(private val view: View) : BaseViewHolder(view) {

    private val currencyText: TextView by lazy {
        view.findViewById<TextView>(R.id.currency_text)
    }
    private val currencyFullText: TextView by lazy {
        view.findViewById<TextView>(R.id.currency_full_text)
    }
    private val amount: EditText by lazy {
        view.findViewById<EditText>(R.id.amount)
    }
    private val countryFlag: ImageView by lazy {
        view.findViewById<ImageView>(R.id.country_flag)
    }

    private val textWatcher = AmountTextWatcher()
    private var maxDecimalPlaces: Int = -1
    private lateinit var amountUpdatedListener: (amount: BigDecimal) -> Unit
    private lateinit var moneyFormatter: MoneyFormatter

    private fun hasValueBeenFormatted(text: Editable?, formattedAmount: String) =
        text.toString().length > formattedAmount.length


    @SuppressLint("SetTextI18n")
    override fun bind(
        item: CurrencyUiModel,
        amountUpdatedListener: ((amount: BigDecimal) -> Unit)?,
        positionSwapListener: ((position: Int, currency: String, amount: BigDecimal) -> Unit)?,
        position: Int,
        imageAlreadySet: Boolean,
        moneyFormatter: MoneyFormatter,
        glide: RequestManager
    ) {
        this.maxDecimalPlaces = item.decimalPlacesCount
        this.amountUpdatedListener = amountUpdatedListener ?: {}
        this.moneyFormatter = moneyFormatter

        currencyText.text = item.currency
        currencyFullText.text = item.currencyFullName

        setCountryFlag(countryFlag, glide, item.currency)

        amount.apply {
            setText(moneyFormatter.getAmount(item))
            requestFocus()
            removeTextChangedListener(textWatcher)
            addTextChangedListener(textWatcher)
        }
    }

    inner class AmountTextWatcher : TextWatcher {

        private var modifiedPosition = 0

        override fun afterTextChanged(text: Editable?) {
            amount.removeTextChangedListener(this)

            val formattedAmount = moneyFormatter.format(text.toString(), maxDecimalPlaces)

            if (hasValueBeenFormatted(text, formattedAmount)) {
                amount.setText(formattedAmount)
                when {
                    maxDecimalPlaces == 0 -> amount.setSelection(modifiedPosition)
                    modifiedPosition < formattedAmount.length ->
                        amount.setSelection(modifiedPosition + 1)
                    else -> amount.setSelection(formattedAmount.length)
                }
            }

            amountUpdatedListener.invoke(BigDecimal(formattedAmount))
            amount.addTextChangedListener(this)
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(text: CharSequence?, position: Int, p2: Int, p3: Int) {
            modifiedPosition = position
        }
    }
}