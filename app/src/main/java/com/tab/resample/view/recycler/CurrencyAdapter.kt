package com.tab.resample.view.recycler

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.bumptech.glide.RequestManager
import com.tab.resample.R
import com.tab.resample.mapper.MoneyFormatter
import com.tab.resample.view.model.CurrencyUiModel
import com.tab.resample.view.recycler.viewholder.BaseViewHolder
import com.tab.resample.view.recycler.viewholder.CurrencyEditableViewHolder
import com.tab.resample.view.recycler.viewholder.CurrencyNotEditableViewHolder
import java.math.BigDecimal
import javax.inject.Inject


class CurrencyAdapter @Inject constructor(
    private val moneyFormatter: MoneyFormatter,
    private val glide: RequestManager,
    diffUtil: CurrencyDiffUtil
) : ListAdapter<CurrencyUiModel, BaseViewHolder>(diffUtil) {

    companion object {
        private const val VIEW_TYPE_EDITABLE = 0
        private const val VIEW_TYPE_NOT_EDITABLE = 1
    }

    private var amountUpdatedListener: ((amount: BigDecimal) -> Unit)? = null
    private var positionSwapListener: ((position: Int, currency: String, amount: BigDecimal) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return if (viewType == VIEW_TYPE_EDITABLE) {
            createEditableViewHolder(parent)
        } else {
            return createNotEditableViewHolder(parent)
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) =
        holder.bind(
            item = getItem(position),
            amountUpdatedListener = amountUpdatedListener,
            positionSwapListener = positionSwapListener,
            position = position,
            imageAlreadySet = false,
            moneyFormatter = moneyFormatter,
            glide = glide
        )


    override fun onBindViewHolder(
        holder: BaseViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) =
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
        } else {
            val bundle = payloads[0] as Bundle
            var imageAlreadySet = false
            for (key in bundle.keySet()) {
                if (key == CurrencyDiffUtil.IMAGE_ALREADY_SET) {
                    imageAlreadySet = bundle.getBoolean(key)
                }
            }
            holder.bind(
                item = getItem(position),
                amountUpdatedListener = amountUpdatedListener,
                positionSwapListener = positionSwapListener,
                position = position,
                imageAlreadySet = imageAlreadySet,
                moneyFormatter = moneyFormatter,
                glide = glide
            )
        }


    override fun getItemViewType(position: Int) =
        if (position == 0) VIEW_TYPE_EDITABLE else VIEW_TYPE_NOT_EDITABLE

    fun setAmountUpdatedListener(onClick: (amount: BigDecimal) -> Unit) {
        amountUpdatedListener = onClick
    }

    fun setPositionSwapListener(onSwap: (position: Int, currency: String, amount: BigDecimal) -> Unit) {
        positionSwapListener = { position, currency, amount ->
            onSwap.invoke(position, currency, amount)

            val updatedList = updateListPosition(position)
            submitList(updatedList)
        }
    }

    private fun updateListPosition(position:Int): ArrayList<CurrencyUiModel> {
        val items = arrayListOf<CurrencyUiModel>()
        for (i in 0 until itemCount) {
            items.add(getItem(i))
        }
        val item = getItem(position)
        items.removeAt(position)
        items.add(0, item)
        return items
    }

    private fun createEditableViewHolder(parent: ViewGroup): CurrencyEditableViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_currency, parent, false)

        return CurrencyEditableViewHolder(view)
    }

    private fun createNotEditableViewHolder(parent: ViewGroup): CurrencyNotEditableViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_currency, parent, false)

        return CurrencyNotEditableViewHolder(view)
    }
}