package com.tab.resample.repository

import com.squareup.moshi.JsonClass
import java.net.HttpURLConnection

@JsonClass(generateAdapter = true)
data class CurrencyResponse(
    val base: String,
    val rates: Map<String, Double>,
    @Transient val errorCode: Int = HttpURLConnection.HTTP_OK
)

fun CurrencyResponse.isSuccess() = errorCode == HttpURLConnection.HTTP_OK