package com.tab.resample.repository

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyApi {
    @GET("latest")
    fun getCurrencyList(@Query("base") base: String): Call<CurrencyResponse>
}