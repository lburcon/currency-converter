package com.tab.resample.repository

import java.io.IOException
import javax.inject.Inject

class CurrencyRepository @Inject constructor(private val api: CurrencyApi) {

    @Throws(IOException::class)
    fun getCurrencyList(currency: String): CurrencyResponse {
        val response = api.getCurrencyList(currency).execute()
        val responseBody = response.body()

        return if (response.isSuccessful && responseBody != null) {
            responseBody
        } else {
            CurrencyResponse("", hashMapOf(), response.code())
        }
    }

}