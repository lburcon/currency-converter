package com.tab.resample.di

import com.tab.resample.view.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class CurrencyBindings {

    @ContributesAndroidInjector(modules = [AppModule::class])
    abstract fun bindMainActivity(): MainActivity
}