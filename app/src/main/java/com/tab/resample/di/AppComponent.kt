package com.tab.resample.di

import com.tab.resample.CurrencyApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        NetworkModule::class,
        CurrencyBindings::class
    ]
)
interface AppComponent {

    fun inject(currencyApplication: CurrencyApplication)

    @Component.Builder
    interface Builder {

        fun build(): AppComponent

        @BindsInstance
        fun application(currencyApplication: CurrencyApplication): Builder
    }
}