package com.tab.resample.di

import android.content.Context
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.tab.resample.repository.CurrencyApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory


@Module
class NetworkModule {

    companion object {
        private const val API_URL = "https://revolut.duckdns.org/"
    }

    @Provides
    fun provideRetrofit(): CurrencyApi {
        val retrofit = Retrofit.Builder()
            .baseUrl(API_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()

        return retrofit.create(CurrencyApi::class.java)
    }

    @Provides
    fun provideGlide(context: Context): RequestManager {
        return Glide.with(context)
    }
}