package com.tab.resample.di

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tab.resample.CurrencyApplication
import com.tab.resample.view.MainActivity
import com.tab.resample.view.ViewContract
import com.tab.resample.viewmodel.CurrencyViewModel
import com.tab.resample.viewmodel.factory.BaseViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class AppModule {

    @Binds
    abstract fun bindContext(application: CurrencyApplication): Context

    @Binds
    abstract fun bindView(activity: MainActivity): ViewContract

    @Binds
    abstract fun bindViewModelFactory(
        viewModelFactory: BaseViewModelFactory
    ): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(CurrencyViewModel::class)
    abstract fun bindMyViewModel(myViewModel: CurrencyViewModel): ViewModel
}