package com.tab.resample.utils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.supervisorScope
import kotlin.coroutines.CoroutineContext

fun CoroutineScope.uiJob(block: suspend CoroutineScope.() -> Unit) {
    startJob(this, Dispatchers.Main, block)
}

private fun startJob(
    parentScope: CoroutineScope,
    coroutineContext: CoroutineContext,
    block: suspend CoroutineScope.() -> Unit
) {
    parentScope.launch(coroutineContext) {
        supervisorScope {
            parentScope.block()
        }
    }
}