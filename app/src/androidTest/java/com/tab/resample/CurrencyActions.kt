package com.tab.resample

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.action.ViewActions.typeTextIntoFocusedView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.*
import com.schibsted.spain.barista.assertion.BaristaListAssertions.assertDisplayedAtPosition
import com.schibsted.spain.barista.assertion.BaristaVisibilityAssertions.assertDisplayed
import com.schibsted.spain.barista.assertion.BaristaVisibilityAssertions.assertNotDisplayed
import com.schibsted.spain.barista.interaction.BaristaListInteractions.clickListItem
import it.xabaras.android.espresso.recyclerviewchildactions.RecyclerViewChildActions.Companion.actionOnChild
import it.xabaras.android.espresso.recyclerviewchildactions.RecyclerViewChildActions.Companion.childOfViewAtPositionWithMatcher
import org.hamcrest.Matchers.not

object CurrencyActions {

    internal fun iSeeCurrencyList() {
        assertDisplayed(R.id.currency_list)
    }

    internal fun iSeeLoadingStatus() {
        assertDisplayed(R.id.loading_status)
    }

    internal fun iDontSeeHeaderText() {
        assertNotDisplayed(R.id.header_text)
    }

    internal fun iSeeCurrencyAtPosition(position: Int, currency: String) {
        assertDisplayedAtPosition(
            R.id.currency_list,
            position,
            R.id.currency_text,
            currency
        )
    }

    internal fun iSeeFullCurrencyAtPosition(position: Int, fullCurrency: String) {
        assertDisplayedAtPosition(
            R.id.currency_list,
            position,
            R.id.currency_full_text,
            fullCurrency
        )
    }

    internal fun iSeeAmountAtPosition(position: Int, amount: String) {
        assertDisplayedAtPosition(
            R.id.currency_list,
            position,
            R.id.amount,
            amount
        )
    }

    internal fun iClickItemAtPosition(position: Int) {
        clickListItem(R.id.currency_list, position)
    }

    internal fun iDontSeeAmountAtPosition(position: Int, amount: String) {
        onView(withId(R.id.currency_list))
            .check(
                matches(
                    not(
                        childOfViewAtPositionWithMatcher(
                            R.id.amount,
                            3,
                            withText("I changed this text via RecyclerViewChildActions")
                        )
                    )
                )
            )
    }

    internal fun iReplaceAmountAtPosition(position: Int, amount: String) {
        onView(withId(R.id.currency_list))
            .perform(
                actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    position,
                    actionOnChild(
                        replaceText(amount),
                        R.id.amount
                    )
                )
            )
    }

    internal fun iClearAmountAtPosition(position: Int) {
        onView(withId(R.id.currency_list))
            .perform(
                actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    position,
                    actionOnChild(
                        replaceText(""),
                        R.id.amount
                    )
                )
            )
    }

    internal fun iWriteAmountAtPosition(position: Int, amount: String) {
        onView(withId(R.id.currency_list))
            .perform(
                actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    position,
                    actionOnChild(
                        typeTextIntoFocusedView(amount),
                        R.id.amount
                    )
                )
            )
    }

    internal fun iSeeCountryFlagAtPosition(position: Int) {
        onView(withId(R.id.currency_list))
            .check(
                matches(
                    childOfViewAtPositionWithMatcher(
                        R.id.country_flag,
                        3,
                        isDisplayed()
                    )

                )
            )
    }
}