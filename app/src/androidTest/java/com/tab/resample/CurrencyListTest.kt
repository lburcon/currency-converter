package com.tab.resample

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.schibsted.spain.barista.assertion.BaristaRecyclerViewAssertions.assertRecyclerViewItemCount
import com.tab.resample.CurrencyActions.iClickItemAtPosition
import com.tab.resample.CurrencyActions.iDontSeeHeaderText
import com.tab.resample.CurrencyActions.iSeeCountryFlagAtPosition
import com.tab.resample.CurrencyActions.iSeeCurrencyAtPosition
import com.tab.resample.CurrencyActions.iSeeCurrencyList
import com.tab.resample.CurrencyActions.iSeeFullCurrencyAtPosition
import com.tab.resample.CurrencyActions.iSeeLoadingStatus
import com.tab.resample.view.MainActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CurrencyListTest : BaseUiTest() {

    companion object {
        private const val LIST_SIZE = 33
    }

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun loadingStatus_displayed() {
        given { iWaitForLoadingStatus() }
        then { iSeeLoadingStatus() }
        and { iDontSeeHeaderText() }
    }

    @Test
    fun list_displayed() {
        given { iWaitForCurrencyList() }
        then { iSeeCurrencyList() }
    }

    @Test
    fun list_hasAllData() {
        given { iWaitForCurrencyList() }
        then { assertRecyclerViewItemCount(R.id.currency_list, LIST_SIZE) }
    }

    @Test
    fun list_hasCorrect_fullCurrencyName() {
        given { iWaitForCurrencyList() }

        then { iSeeCurrencyAtPosition(0, "EUR") }
        and { iSeeFullCurrencyAtPosition(0, "Euro") }
        and { iSeeCurrencyAtPosition(1, "AUD") }
        and { iSeeFullCurrencyAtPosition(1, "Australian Dollar") }
    }

    @Test
    fun list_imagesDisplayed() {
        given { iWaitForCurrencyList() }

        and { iSeeCountryFlagAtPosition(0); }
        and { iSeeCountryFlagAtPosition(1); }
        and { iSeeCountryFlagAtPosition(LIST_SIZE - 1); }
    }

    @Test
    fun list_scrollTo_lastItem() {
        given { iWaitForCurrencyList() }
        then { iSeeCurrencyAtPosition(LIST_SIZE - 1, "ZAR") }
    }

    @Test
    fun list_clickSecondItem() {
        given { iWaitForCurrencyList() }
        and { iSeeCurrencyAtPosition(0, "EUR") }

        whenever { iClickItemAtPosition(1) }

        then { iSeeCurrencyAtPosition(0, "AUD") }
        and { iSeeCurrencyAtPosition(1, "EUR") }
    }

    @Test
    fun list_clickSecondItem_thenThird() {
        given { iWaitForCurrencyList() }
        and { iSeeCurrencyAtPosition(0, "EUR") }

        whenever { iClickItemAtPosition(1) }

        then { iSeeCurrencyAtPosition(0, "AUD") }
        and { iSeeCurrencyAtPosition(1, "EUR") }

        whenever { iClickItemAtPosition(2) }

        then { iSeeCurrencyAtPosition(0, "BGN") }
        and { iSeeCurrencyAtPosition(1, "AUD") }
        and { iSeeCurrencyAtPosition(2, "EUR") }
    }

    @Test
    fun list_clickLastItem() {
        given { iWaitForCurrencyList() }
        and { iSeeCurrencyAtPosition(0, "EUR") }

        whenever { iClickItemAtPosition(LIST_SIZE - 1) }

        then { iSeeCurrencyAtPosition(0, "ZAR") }
        and { iSeeCurrencyAtPosition(1, "EUR") }
    }

}