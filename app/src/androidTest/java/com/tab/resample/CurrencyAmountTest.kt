package com.tab.resample

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.tab.resample.CurrencyActions.iClearAmountAtPosition
import com.tab.resample.CurrencyActions.iClickItemAtPosition
import com.tab.resample.CurrencyActions.iDontSeeAmountAtPosition
import com.tab.resample.CurrencyActions.iReplaceAmountAtPosition
import com.tab.resample.CurrencyActions.iSeeAmountAtPosition
import com.tab.resample.CurrencyActions.iSeeCurrencyAtPosition
import com.tab.resample.CurrencyActions.iWriteAmountAtPosition
import com.tab.resample.view.MainActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CurrencyAmountTest : BaseUiTest() {

    companion object {
        private const val DEFAULT_AMOUNT = ""
    }

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun writeAmount_to_firstItem() {
        given { iWaitForCurrencyList() }

        whenever { iReplaceAmountAtPosition(0, "20.0") }

        then { iSeeAmountAtPosition(0, "20.0") }
    }

    @Test
    fun typeValue_wait_addDecimalPart() {
        given { iWaitForCurrencyList() }

        whenever { iClearAmountAtPosition(0) }
        and { iWriteAmountAtPosition(0, "5") }

        then { iSeeAmountAtPosition(0, "5") }

        whenever { iWriteAmountAtPosition(0, ".12") }

        then { iSeeAmountAtPosition(0, "5.12") }
    }

    @Test
    fun typeValue_otherFieldsUpdated() {
        given { iWaitForCurrencyList() }
        and { iSeeAmountAtPosition(1, DEFAULT_AMOUNT) }
        and { iSeeAmountAtPosition(10, DEFAULT_AMOUNT) }

        and { iWriteAmountAtPosition(0, "50") }

        then { iDontSeeAmountAtPosition(1, DEFAULT_AMOUNT) }
        and { iDontSeeAmountAtPosition(10, DEFAULT_AMOUNT) }
    }

    @Test
    fun typeValue_changeCurrency_valueRetained() {
        given { iWaitForCurrencyList() }
        and { iSeeAmountAtPosition(5, DEFAULT_AMOUNT) }

        whenever { iReplaceAmountAtPosition(0, "50.00") }
        and { iClickItemAtPosition(5) }

        then { iDontSeeAmountAtPosition(0, DEFAULT_AMOUNT) }
        and { iDontSeeAmountAtPosition(1, DEFAULT_AMOUNT) }
    }

    @Test
    fun typeValue_changeCurrency_correctPositions() {
        given { iWaitForCurrencyList() }
        and { iSeeCurrencyAtPosition(0, "EUR") }
        and { iSeeCurrencyAtPosition(5, "CHF") }

        whenever { iReplaceAmountAtPosition(0, "50.00") }
        and { iClickItemAtPosition(5) }

        then { iSeeCurrencyAtPosition(0, "CHF") }
        and { iSeeCurrencyAtPosition(1, "EUR") }
    }

    @Test
    fun changeCurrency_typeAmount() {
        given { iWaitForCurrencyList() }

        whenever { iClickItemAtPosition(5) }
        and { iWaitForCurrencyChanged("CHF") }
        and { iReplaceAmountAtPosition(0, "50.00") }

        then { iSeeCurrencyAtPosition(0, "CHF") }
        and { iSeeAmountAtPosition(0, "50.00") }
    }

    @Test
    fun changeCurrency_typeValue_thenDecimal() {
        given { iWaitForCurrencyList() }

        whenever { iClickItemAtPosition(5) }
        and { iWaitForCurrencyChanged("CHF") }
        and { iClearAmountAtPosition(0) }
        and { iWriteAmountAtPosition(0, "5") }

        then { iSeeCurrencyAtPosition(0, "CHF") }
        and { iSeeAmountAtPosition(0, "5") }

        whenever { iWriteAmountAtPosition(0, ".12") }

        then { iSeeAmountAtPosition(0, "5.12") }
    }

    @Test
    fun enterAmount_manyDigits() {
        given { iWaitForCurrencyList() }

        whenever { iWriteAmountAtPosition(0, "123456789123456123456789123456123456789123456") }
        and { iWriteAmountAtPosition(0, "7") }

        then { iSeeAmountAtPosition(0, "1234567891234561234567891234561234567891234567") }
    }

    @Test
    fun enterAmount_maxDigitsReached_WithDecimal() {
        given { iWaitForCurrencyList() }

        whenever { iWriteAmountAtPosition(0, "123456789123.45") }
        and { iWriteAmountAtPosition(0, "6") }

        then { iSeeAmountAtPosition(0, "123456789123.45") }
    }

    @Test
    fun enterThirdDecimal_toAmount_withTwoDecimals() {
        given { iWaitForCurrencyList() }

        whenever { iWriteAmountAtPosition(0, "5.12") }
        and { iWriteAmountAtPosition(0, "1") }

        then { iSeeAmountAtPosition(0, "5.12") }
    }

    @Test
    fun enterSecondDecimalSeparator_toAmount_withDecimals() {
        given { iWaitForCurrencyList() }

        whenever { iWriteAmountAtPosition(0, "5.1") }
        and { iWriteAmountAtPosition(0, ".1") }

        then { iSeeAmountAtPosition(0, "5.11") }
    }

    @Test
    fun enterDecimal_toAmount_withZeroDecimals() {
        given { iWaitForCurrencyList() }

        whenever { iClickItemAtPosition(17) }
        and { iWaitForCurrencyChanged("JPY") }
        and { iWriteAmountAtPosition(0, "50") }
        and { iWriteAmountAtPosition(0, ".") }

        then { iSeeAmountAtPosition(0, "50") }
    }
}