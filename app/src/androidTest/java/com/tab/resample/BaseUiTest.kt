package com.tab.resample

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewAssertion
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.azimolabs.conditionwatcher.ConditionWatcher
import com.azimolabs.conditionwatcher.Instruction
import it.xabaras.android.espresso.recyclerviewchildactions.RecyclerViewChildActions
import junit.framework.AssertionFailedError

abstract class BaseUiTest {

    fun iWaitForCondition(
        viewInteraction: ViewInteraction,
        viewAssertion: ViewAssertion
    ) =
        ConditionWatcher.waitForCondition(object : Instruction() {
            override fun getDescription(): String {
                return "Waiting for viewAssertion"
            }

            override fun checkCondition(): Boolean {
                return try {
                    viewInteraction.check { view, _ -> viewAssertion.check(view, null) }
                    true
                } catch (ignored: AssertionFailedError) {
                    false
                }
            }
        })

    fun iWaitForCurrencyList() {
        iWaitForCondition(
            onView(withId(R.id.currency_list)),
            matches(isDisplayed())
        )
    }

    fun iWaitForLoadingStatus() {
        iWaitForCondition(
            onView(withId(R.id.loading_status)),
            matches(isDisplayed())
        )
    }

    internal fun iWaitForCurrencyChanged(newCurrency: String) {
        iWaitForCondition(
            onView(withId(R.id.currency_list)),
            matches(
                RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                    R.id.currency_text,
                    0,
                    ViewMatchers.withText(newCurrency)
                )
            )
        )
    }

    fun given(action: () -> Unit) {
        action()
    }

    fun whenever(action: () -> Unit) {
        action()
    }

    fun then(action: () -> Unit) {
        action()
    }

    fun and(action: () -> Unit) {
        action()
    }

}