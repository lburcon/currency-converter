package com.tab.resample.repository

import com.tab.resample.CurrencyStubs
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.net.HttpURLConnection

@RunWith(MockitoJUnitRunner::class)
class CurrencyRepositoryTest {

    companion object {
        private const val BASE_CURRENCY = "EUR"
    }

    @Mock
    lateinit var mockApi: CurrencyApi

    lateinit var subject: CurrencyRepository

    @Before
    fun setup() {
        subject = CurrencyRepository(mockApi)
    }

    @Test
    fun `getCurrencyList() - successful response`() {
        val currency = BASE_CURRENCY
        val response = CurrencyStubs.createCurrencyResponse()
        val mockCall = ApiMockCall.buildSuccess(response)
        given(mockApi.getCurrencyList(currency)).willReturn(mockCall)

        val result = subject.getCurrencyList(currency)

        assertThat(result.base).isEqualTo(BASE_CURRENCY)
        assertThat(result.rates).hasSize(2)
    }

    @Test
    fun `getCurrencyList() - unsuccessful response HTTP_FORBIDDEN`() {
        val currency = BASE_CURRENCY
        val response =
            CurrencyStubs.createCurrencyResponse("", emptyMap(), HttpURLConnection.HTTP_FORBIDDEN)
        val mockCall = ApiMockCall.buildSuccess(response)
        given(mockApi.getCurrencyList(currency)).willReturn(mockCall)

        val result = subject.getCurrencyList(currency)

        assertThat(result.base).isEmpty()
        assertThat(result.rates).isEmpty()
        assertThat(result.errorCode).isEqualTo(HttpURLConnection.HTTP_FORBIDDEN)
    }
}