package com.tab.resample

import com.tab.resample.repository.CurrencyResponse
import com.tab.resample.view.model.CurrencyUiModel
import java.math.BigDecimal
import java.net.HttpURLConnection

object CurrencyStubs {

    const val BASE_CURRENCY = "EUR"
    const val FIRST_CURRENCY = "PLN"
    const val FIRST_CURRENCY_FULL_NAME = "Polish Zloty"
    const val SECOND_CURRENCY_FULL_NAME = "US Dollar"
    const val DECIMAL_PLACES_TWO = 2
    const val FIRST_RATE = 4.4321
    const val SECOND_CURRENCY = "USD"
    const val SECOND_RATE = 1.1234
    private val FIRST_CALCULATED_AMOUNT = BigDecimal(1234.1234)
    private val UPDATED_FIRST_CALCULATED_AMOUNT = BigDecimal(123.1234)
    private val SECOND_CALCULATED_AMOUNT = BigDecimal(4321.1234)
    private val UPDATED_SECOND_CALCULATED_AMOUNT = BigDecimal(432.1234)

    private val currencyMap = hashMapOf(FIRST_CURRENCY to FIRST_RATE, SECOND_CURRENCY to SECOND_RATE)

    private val currencyResponse = CurrencyResponse(
            BASE_CURRENCY,
            currencyMap
    )

    private val currencyUiModel = CurrencyUiModel(
        FIRST_CURRENCY,
        FIRST_CURRENCY_FULL_NAME,
        FIRST_CALCULATED_AMOUNT,
        DECIMAL_PLACES_TWO
    )

    private val currencyUiModelList =
            listOf(
                CurrencyUiModel(
                    FIRST_CURRENCY,
                    FIRST_CURRENCY_FULL_NAME,
                    FIRST_CALCULATED_AMOUNT,
                    DECIMAL_PLACES_TWO
                ), CurrencyUiModel(
                    SECOND_CURRENCY,
                    SECOND_CURRENCY_FULL_NAME,
                    SECOND_CALCULATED_AMOUNT,
                    DECIMAL_PLACES_TWO
                )
            )
    private val updatedCurrencyUiModelList =
            listOf(
                    currencyUiModelList[0].copy(calculatedAmount = UPDATED_FIRST_CALCULATED_AMOUNT),
                    currencyUiModelList[1].copy(calculatedAmount = UPDATED_SECOND_CALCULATED_AMOUNT)
            )

    fun createCurrencyResponse(
            currency: String = BASE_CURRENCY,
            rates: Map<String, Double> = currencyMap,
            errorCode: Int = HttpURLConnection.HTTP_OK
    ) = currencyResponse.copy(
            base = currency,
            rates = rates,
            errorCode = errorCode
    )

    fun createCurrencyUiModel() = currencyUiModel.copy()
    fun createCurrencyUiModelList() = currencyUiModelList.toList()
    fun createUpdatedCurrencyUiModelList() = updatedCurrencyUiModelList.toList()
    fun createCurrencyMap() = currencyMap.toMap()
}