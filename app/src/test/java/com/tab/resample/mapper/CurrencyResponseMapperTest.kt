package com.tab.resample.mapper

import com.tab.resample.CurrencyStubs
import com.tab.resample.CurrencyStubs.FIRST_CURRENCY
import com.tab.resample.CurrencyStubs.SECOND_CURRENCY
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class CurrencyResponseMapperTest {

    companion object {
        private const val DEFAULT_AMOUNT = 1.0
        private const val DEFAULT_VALUE = "-"
    }

    private val subject = CurrencyResponseMapper()

    @Test
    fun `map() - success`() {
        val currencyResponse = CurrencyStubs.createCurrencyResponse()
        val responseMap = currencyResponse.rates
        val responseBase = currencyResponse.base

        val result = subject.mapToMap(currencyResponse)

        assertThat(result.size).isEqualTo(3)
        assertThat(result[responseBase]).isEqualTo(DEFAULT_AMOUNT)
        assertThat(result[FIRST_CURRENCY]).isEqualTo(responseMap[FIRST_CURRENCY])
        assertThat(result[SECOND_CURRENCY]).isEqualTo(responseMap[SECOND_CURRENCY])
    }

    @Test
    fun `map() - base currency only`() {
        val currencyResponse = CurrencyStubs.createCurrencyResponse(rates = emptyMap())
        val responseBase = currencyResponse.base

        val result = subject.mapToMap(currencyResponse)

        assertThat(result.size).isEqualTo(1)
        assertThat(result[responseBase]).isEqualTo(DEFAULT_AMOUNT)
    }

    @Test
    fun `map() - empty value`() {
        val currencyResponse =
            CurrencyStubs.createCurrencyResponse(currency = "", rates = emptyMap())

        val result = subject.mapToMap(currencyResponse)

        assertThat(result.size).isEqualTo(1)
        assertThat(result[DEFAULT_VALUE]).isEqualTo(DEFAULT_AMOUNT)
    }
}