package com.tab.resample.mapper

import com.tab.resample.CurrencyStubs
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.math.BigDecimal
import java.math.RoundingMode

class MoneyFormatterTest {

    companion object {
        private const val DEFAULT_AMOUNT = "0"
        private const val DEFAULT_VALUE = ""
    }

    private val subject = MoneyFormatter()

    @Test
    fun `format() - decimals not allowed without decimal digits`() {
        val amount = "10"
        val decimalPlaces = 0

        val result = subject.format(amount, decimalPlaces)

        assertThat(result).isEqualTo(amount)
    }

    @Test
    fun `format() - decimals not allowed with decimal separator`() {
        val amount = "10."
        val decimalPlaces = 0
        val formattedAmount = "10"

        val result = subject.format(amount, decimalPlaces)

        assertThat(result).isEqualTo(formattedAmount)
    }

    @Test
    fun `format() - one decimal allowed without decimal digits`() {
        val amount = "10"
        val decimalPlaces = 1
        val formattedAmount = "10"

        val result = subject.format(amount, decimalPlaces)

        assertThat(result).isEqualTo(formattedAmount)
    }

    @Test
    fun `format() - one decimal allowed with decimal separator`() {
        val amount = "10."
        val decimalPlaces = 1
        val formattedAmount = "10."

        val result = subject.format(amount, decimalPlaces)

        assertThat(result).isEqualTo(formattedAmount)
    }

    @Test
    fun `format() - one decimal allowed with one decimal digit`() {
        val amount = "10.1"
        val decimalPlaces = 1
        val formattedAmount = "10.1"

        val result = subject.format(amount, decimalPlaces)

        assertThat(result).isEqualTo(formattedAmount)
    }

    @Test
    fun `format() - one decimal allowed with two decimal digits`() {
        val amount = "10.12"
        val decimalPlaces = 1
        val formattedAmount = "10.1"

        val result = subject.format(amount, decimalPlaces)

        assertThat(result).isEqualTo(formattedAmount)
    }

    @Test
    fun `format() - one decimal allowed with three decimal digits`() {
        val amount = "10.123"
        val decimalPlaces = 1
        val formattedAmount = "10.1"

        val result = subject.format(amount, decimalPlaces)

        assertThat(result).isEqualTo(formattedAmount)
    }

    @Test
    fun `format() - two decimal allowed without decimal digits`() {
        val amount = "10"
        val decimalPlaces = 2
        val formattedAmount = "10"

        val result = subject.format(amount, decimalPlaces)

        assertThat(result).isEqualTo(formattedAmount)
    }

    @Test
    fun `format() - two decimal allowed with decimal number separator`() {
        val amount = "10."
        val decimalPlaces = 2
        val formattedAmount = "10."

        val result = subject.format(amount, decimalPlaces)

        assertThat(result).isEqualTo(formattedAmount)
    }

    @Test
    fun `format() - two decimal allowed with one decimal digit`() {
        val amount = "10.1"
        val decimalPlaces = 2
        val formattedAmount = "10.1"

        val result = subject.format(amount, decimalPlaces)

        assertThat(result).isEqualTo(formattedAmount)
    }

    @Test
    fun `format() - two decimal allowed with two decimal digits`() {
        val amount = "10.12"
        val decimalPlaces = 2
        val formattedAmount = "10.12"

        val result = subject.format(amount, decimalPlaces)

        assertThat(result).isEqualTo(formattedAmount)
    }

    @Test
    fun `format() - two decimal allowed with three decimal digits`() {
        val amount = "10.123"
        val decimalPlaces = 2
        val formattedAmount = "10.12"

        val result = subject.format(amount, decimalPlaces)

        assertThat(result).isEqualTo(formattedAmount)
    }

    @Test
    fun `format() - two decimal allowed with four decimal digits`() {
        val amount = "10.1234"
        val decimalPlaces = 2
        val formattedAmount = "10.12"

        val result = subject.format(amount, decimalPlaces)

        assertThat(result).isEqualTo(formattedAmount)
    }

    @Test
    fun `format() - empty amount`() {
        val amount = ""
        val decimalPlaces = 2

        val result = subject.format(amount, decimalPlaces)

        assertThat(result).isEqualTo(DEFAULT_AMOUNT)
    }

    @Test
    fun getAmount() {
        val currencyUiModel = CurrencyStubs.createCurrencyUiModel()
        val roundedCalculatedAmount = currencyUiModel.calculatedAmount.setScale(2, RoundingMode.HALF_UP).toString()

        val result = subject.getAmount(currencyUiModel)

        assertThat(result).isEqualTo(roundedCalculatedAmount)
    }

    @Test
    fun `getAmount() with calculated amount zero`() {
        val currencyUiModel =
                CurrencyStubs.createCurrencyUiModel().copy(calculatedAmount = BigDecimal.ZERO)

        val result = subject.getAmount(currencyUiModel)

        assertThat(result).isEqualTo(DEFAULT_VALUE)
    }
}