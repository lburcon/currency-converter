package com.tab.resample.view.recycler

import com.tab.resample.view.model.CurrencyUiModel
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.math.BigDecimal

class CurrencyDiffUtilTest {

    private val subject = CurrencyDiffUtil()
    private val oldItem =
        CurrencyUiModel("EUR", "Euro", BigDecimal(10), 2, false)
    private val newItem =
        CurrencyUiModel("USD", "US Dollar", BigDecimal(300), 2, false)

    @Test
    fun `areContentsTheSame() - different items`() {

        val result = subject.areContentsTheSame(oldItem, newItem)
        assertThat(result).isFalse()
    }

    @Test
    fun `areContentsTheSame() - the same items items`() {

        val result = subject.areContentsTheSame(oldItem, oldItem.copy())
        assertThat(result).isTrue()
    }

    @Test
    fun `areContentsTheSame() - different items, but first position`() {

        val result = subject.areContentsTheSame(oldItem, oldItem.copy(isFirstItem = true))
        assertThat(result).isTrue()
    }

    @Test
    fun `areItemsTheSame() - the same items with different value`() {
        val result = subject.areItemsTheSame(oldItem, oldItem.copy(calculatedAmount = BigDecimal(200)))
        assertThat(result).isTrue()
    }

    @Test
    fun `areItemsTheSame() - different items`() {

        val result = subject.areItemsTheSame(oldItem, newItem)
        assertThat(result).isFalse()
    }
}