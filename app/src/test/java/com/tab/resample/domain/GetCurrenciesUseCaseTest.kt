package com.tab.resample.domain

import com.tab.resample.CurrencyStubs
import com.tab.resample.mapper.CurrencyResponseMapper
import com.tab.resample.repository.CurrencyRepository
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.io.IOException
import java.net.HttpURLConnection
import java.net.SocketTimeoutException

@RunWith(MockitoJUnitRunner::class)
class GetCurrenciesUseCaseTest {

    companion object {
        private const val BASE_CURRENCY = "EUR"
    }

    @Mock
    lateinit var mockCurrencyRepository: CurrencyRepository
    @Mock
    lateinit var mockCurrencyResponseMapper: CurrencyResponseMapper

    private lateinit var subject: GetCurrenciesUseCase

    @Before
    fun setup() {
        subject = GetCurrenciesUseCase(mockCurrencyRepository, mockCurrencyResponseMapper)
    }

    @Test
    fun `execute() - successful`() {
        val currency = BASE_CURRENCY
        val response = CurrencyStubs.createCurrencyResponse()
        val currencyModelList = CurrencyStubs.createCurrencyMap()
        given(mockCurrencyRepository.getCurrencyList(currency)).willReturn(response)
        given(mockCurrencyResponseMapper.mapToMap(response)).willReturn(currencyModelList)

        val result = runBlocking { subject.execute(currency) }

        assertThat(result).isInstanceOf(GetCurrenciesUseCase.UseCaseResult.Success::class.java)
        result as GetCurrenciesUseCase.UseCaseResult.Success
        assertThat(result.data).isEqualTo(currencyModelList)
    }

    @Test
    fun `execute() - successful call with error code`() {
        val currency = BASE_CURRENCY
        val response =
            CurrencyStubs.createCurrencyResponse(errorCode = HttpURLConnection.HTTP_FORBIDDEN)
        given(mockCurrencyRepository.getCurrencyList(currency)).willReturn(response)

        val result = runBlocking { subject.execute(currency) }

        assertThat(result).isInstanceOf(GetCurrenciesUseCase.UseCaseResult.UnknownError::class.java)
    }

    @Test
    fun `execute() - network error`() {
        val currency = BASE_CURRENCY
        given(mockCurrencyRepository.getCurrencyList(currency)).willThrow(SocketTimeoutException::class.java)

        val result = runBlocking { subject.execute(currency) }

        assertThat(result).isInstanceOf(GetCurrenciesUseCase.UseCaseResult.NetworkError::class.java)
    }

    @Test
    fun `execute() - unknown error`() {
        val currency = BASE_CURRENCY
        given(mockCurrencyRepository.getCurrencyList(currency)).willThrow(IOException::class.java)

        val result = runBlocking { subject.execute(currency) }

        assertThat(result).isInstanceOf(GetCurrenciesUseCase.UseCaseResult.UnknownError::class.java)
    }


}