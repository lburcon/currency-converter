package com.tab.resample.domain

import com.tab.resample.CurrencyStubs
import com.tab.resample.mapper.AmountUiMapper
import com.tab.resample.mapper.MoneyFormatter
import com.tab.resample.view.model.CurrencyUiModel
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.math.BigDecimal

@RunWith(MockitoJUnitRunner::class)
class AmountUiMapperTest {

    companion object {
        private const val DECIMAL_PLACES_ONE = 1
        private const val DECIMAL_PLACES_TWO = 2
    }

    @Mock
    lateinit var mockMoneyFormatter: MoneyFormatter

    private lateinit var subject: AmountUiMapper

    @Before
    fun setup() {
        subject = AmountUiMapper(mockMoneyFormatter)
    }

    private val firstCurrency = CurrencyStubs.FIRST_CURRENCY
    private val firstCurrencyFullName = CurrencyStubs.FIRST_CURRENCY_FULL_NAME
    private val secondCurrency = CurrencyStubs.SECOND_CURRENCY
    private val secondCurrencyFullName = CurrencyStubs.SECOND_CURRENCY_FULL_NAME

    @Test
    fun `execute() - map values`() {
        given(mockMoneyFormatter.getFullCurrencyName(firstCurrency)).willReturn(firstCurrencyFullName)
        given(mockMoneyFormatter.getFullCurrencyName(secondCurrency)).willReturn(secondCurrencyFullName)
        given(mockMoneyFormatter.getDecimalPlaces(firstCurrency)).willReturn(DECIMAL_PLACES_ONE)
        given(mockMoneyFormatter.getDecimalPlaces(secondCurrency)).willReturn(DECIMAL_PLACES_TWO)
        val amount = BigDecimal(20.0)
        val currentCurrency = CurrencyStubs.FIRST_CURRENCY
        val currencyUiList = CurrencyStubs.createCurrencyUiModelList()
        val newCurrencyMap = CurrencyStubs.createCurrencyMap()
        val secondCalculatedAmount = amount.times(BigDecimal(CurrencyStubs.SECOND_RATE))

        val result = runBlocking {
            subject.map(
                    amount,
                    currentCurrency,
                    currencyUiList,
                    newCurrencyMap
            )
        }

        assertThat(result.size).isEqualTo(2)
        assertThat(result[0].currency).isEqualTo(firstCurrency)
        assertThat(result[0].currencyFullName).isEqualTo(firstCurrencyFullName)
        assertThat(result[0].calculatedAmount).isEqualTo(amount)
        assertThat(result[0].decimalPlacesCount).isEqualTo(1)
        assertThat(result[0].isFirstItem).isEqualTo(true)
        assertThat(result[1].currency).isEqualTo(secondCurrency)
        assertThat(result[1].currencyFullName).isEqualTo(secondCurrencyFullName)
        assertThat(result[1].calculatedAmount).isEqualTo(secondCalculatedAmount)
        assertThat(result[1].decimalPlacesCount).isEqualTo(2)
        assertThat(result[1].isFirstItem).isEqualTo(false)
    }

    @Test
    fun `execute() - map values with zero amount`() {
        given(mockMoneyFormatter.getFullCurrencyName(firstCurrency)).willReturn(firstCurrencyFullName)
        given(mockMoneyFormatter.getFullCurrencyName(secondCurrency)).willReturn(secondCurrencyFullName)
        val amount = BigDecimal.ZERO
        val currentCurrency = CurrencyStubs.FIRST_CURRENCY
        val currencyUiList = CurrencyStubs.createCurrencyUiModelList()
        val newCurrencyMap = CurrencyStubs.createCurrencyMap()

        val result = runBlocking {
            subject.map(
                    amount,
                    currentCurrency,
                    currencyUiList,
                    newCurrencyMap
            )
        }

        assertThat(result.size).isEqualTo(2)
        assertThat(result[0].currency).isEqualTo(firstCurrency)
        assertThat(result[0].calculatedAmount).isEqualTo(amount)
        assertThat(result[1].currency).isEqualTo(secondCurrency)
        assertThat(result[1].calculatedAmount).isEqualTo(amount)
    }

    @Test
    fun `execute() - map with empty list returned from the server`() {
        val currencyUiList = CurrencyStubs.createCurrencyUiModelList()
        val newCurrencyMap = emptyMap<String, Double>()
        val amount = BigDecimal(20.0)
        val currentCurrency = CurrencyStubs.BASE_CURRENCY

        val result = runBlocking {
            subject.map(
                    amount,
                    currentCurrency,
                    currencyUiList,
                    newCurrencyMap
            )
        }

        assertThat(result.size).isEqualTo(2)
        assertThat(result).isEqualTo(currencyUiList)
    }

    @Test
    fun `execute() - map empty values`() {
        val currencyUiList = emptyList<CurrencyUiModel>()
        val newCurrencyMap = emptyMap<String, Double>()
        val amount = BigDecimal(20.0)
        val currentCurrency = CurrencyStubs.BASE_CURRENCY

        val result = runBlocking {
            subject.map(
                    amount,
                    currentCurrency,
                    currencyUiList,
                    newCurrencyMap
            )
        }

        assertThat(result.size).isEqualTo(0)
    }
}