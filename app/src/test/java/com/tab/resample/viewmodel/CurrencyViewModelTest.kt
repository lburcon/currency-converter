package com.tab.resample.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.tab.resample.CurrencyStubs
import com.tab.resample.domain.GetCurrenciesUseCase
import com.tab.resample.mapper.AmountUiMapper
import com.tab.resample.view.model.CurrencyUiModel
import com.tab.resample.view.ViewContract
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.*
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.math.BigDecimal

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class CurrencyViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var mockView: ViewContract
    @Mock
    lateinit var mockGetCurrenciesUseCase: GetCurrenciesUseCase
    @Mock
    lateinit var mockAmountUiMapper: AmountUiMapper

    private lateinit var subject: CurrencyViewModel

    private val testObserver = TestObserver<List<CurrencyUiModel>>()
    private val amount = BigDecimal.ZERO


    @Before
    fun setup() {
        subject = CurrencyViewModel(mockView, mockGetCurrenciesUseCase, mockAmountUiMapper)
        Dispatchers.setMain(Dispatchers.Unconfined)
    }

    @After
    fun teardown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `getCurrencyList() - success`() {
        runBlockingTest {
            val data = CurrencyStubs.createCurrencyMap()
            val currency = CurrencyStubs.BASE_CURRENCY

            val currencyUiModelList = emptyList<CurrencyUiModel>()
            val newCurrencyUiModelList = CurrencyStubs.createCurrencyUiModelList()
            given(mockGetCurrenciesUseCase.execute(currency)).willReturn(
                    GetCurrenciesUseCase.UseCaseResult.Success(
                            data
                    )
            )
            given(mockAmountUiMapper.map(amount, currency, currencyUiModelList, data)).willReturn(
                    newCurrencyUiModelList
            )
            subject.getLiveData().observeForever(testObserver)
            subject.getCurrencyList()


            assertThat(testObserver.values.last()).isEqualTo(newCurrencyUiModelList)
        }
    }

    @Test
    fun `getCurrencyList() - network error`() {
        runBlockingTest {
            val currency = CurrencyStubs.BASE_CURRENCY
            given(mockGetCurrenciesUseCase.execute(currency)).willReturn(GetCurrenciesUseCase.UseCaseResult.NetworkError)

            subject.getCurrencyList()

            then(mockView).should().onNetworkError()
        }
    }

    @Test
    fun `getCurrencyList() - unknown error`() {
        runBlockingTest {
            val currency = CurrencyStubs.BASE_CURRENCY
            given(mockGetCurrenciesUseCase.execute(currency)).willReturn(GetCurrenciesUseCase.UseCaseResult.UnknownError)

            subject.getCurrencyList()

            then(mockView).shouldHaveZeroInteractions()
        }
    }

    @Test
    fun `amountUpdated() - got currency list, then amount updated`() {
        runBlockingTest {
            val data = CurrencyStubs.createCurrencyMap()
            val currency = CurrencyStubs.BASE_CURRENCY

            val updatedAmount = BigDecimal(30.0)
            val currencyUiModelList = CurrencyStubs.createCurrencyUiModelList()
            val updatedCurrencyUiModelList = CurrencyStubs.createUpdatedCurrencyUiModelList()
            given(mockGetCurrenciesUseCase.execute(currency))
                    .willReturn(GetCurrenciesUseCase.UseCaseResult.Success(data))
            given(mockAmountUiMapper.map(amount, currency, emptyList(), data))
                    .willReturn(currencyUiModelList)
            given(mockAmountUiMapper.map(updatedAmount, currency, currencyUiModelList, data))
                    .willReturn(updatedCurrencyUiModelList)

            subject.getLiveData().observeForever(testObserver)
            subject.getCurrencyList()

            assertThat(testObserver.values.last()).isEqualTo(currencyUiModelList)

            subject.amountUpdated(updatedAmount)

            assertThat(testObserver.values.last()).isEqualTo(updatedCurrencyUiModelList)
        }
    }

    @Test
    fun `currencyChanged() - gets currency list, then currency position changed correctly`() {
        runBlockingTest {
            val currencyUiModelList = CurrencyStubs.createCurrencyUiModelList()
            val data = CurrencyStubs.createCurrencyMap()
            val position = 1
            val oldCurrency = "EUR"
            val newCurrency = "AUD"
            val oldAmount = BigDecimal(0)
            val newAmount = BigDecimal(20)
            given(mockGetCurrenciesUseCase.execute(oldCurrency))
                    .willReturn(GetCurrenciesUseCase.UseCaseResult.Success(data))
            given(mockAmountUiMapper.map(oldAmount, oldCurrency, emptyList(), data))
                    .willReturn(currencyUiModelList)

            subject.getLiveData().observeForever(testObserver)
            subject.getCurrencyList()
            subject.currencyChanged(position, newCurrency, newAmount)

            then(mockGetCurrenciesUseCase).should().execute(newCurrency)
            assertThat(testObserver.values.last()[0]).isEqualTo(currencyUiModelList[position])
            assertThat(testObserver.values.last()[1]).isEqualTo(currencyUiModelList.first())
        }
    }


    @Test
    fun `currencyChanged() - empty list`() {
        runBlockingTest {
            val position = 1
            val newCurrency = "AUD"
            val newAmount = BigDecimal(20)

            subject.getCurrencyList()
            subject.currencyChanged(position, newCurrency, newAmount)

            then(mockGetCurrenciesUseCase).should().execute(newCurrency)
        }
    }


}