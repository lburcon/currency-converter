package com.tab.resample.viewmodel

import androidx.lifecycle.Observer

class TestObserver<T> : Observer<T> {

    private val _values = mutableListOf<T>()
    val values: List<T>
        get() = _values

    override fun onChanged(value: T) {
        _values.add(value)
    }
}